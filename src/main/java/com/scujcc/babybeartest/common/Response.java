package com.scujcc.babybeartest.common;

import com.scujcc.babybeartest.exception.BabyExceptionEnum;
import lombok.Data;

import java.util.Map;

/**
 * @Description:
 * @Auther: llang
 */

@Data
public class Response<T>{
    private Integer code;
    private String msg;
    private T data;
    public static Integer SUCCESS=0;

    public Response(){
        code=Response.SUCCESS;
        msg="success";
    }

    public Response(T data){
        code=Response.SUCCESS;
        msg="success";
        this.data=data;
    }

    public Response(Integer code,String msg){
        this.code=code;
        this.msg=msg;
    }

    public static Response error(Integer code,String msg){
        return new Response(code,msg);
    }

    public static Response error(BabyExceptionEnum babyExceptionEnum){
        return error(babyExceptionEnum.getCode(),babyExceptionEnum.getMsg());
    }

    public static Response success(Object data){
        return new Response(data);
    }

    public static Response success(){
        return new Response();
    }

}

