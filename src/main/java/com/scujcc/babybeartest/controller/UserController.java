package com.scujcc.babybeartest.controller;

import com.scujcc.babybeartest.common.Response;
import com.scujcc.babybeartest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Date:2022-11-11-16:36
 * @author:Esan
 */
@RestController
@RequestMapping("/babybeartest")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/login")
    public Response login(String username, String password){
        Map<String,String> map=new HashMap<>();
        map.put(username,password);
        return Response.success(map);
    }


    @GetMapping("/logout")
    public String logout(){
        return  "logout";
    }




}
