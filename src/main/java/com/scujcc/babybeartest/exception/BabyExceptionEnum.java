package com.scujcc.babybeartest.exception;

/**
 * @Description:
 * @Auther: llang
 */

public enum BabyExceptionEnum {

    SYSTEM_ERROR(100001,"系统异常");
    private Integer code;
    private String msg;

    BabyExceptionEnum(){}

    BabyExceptionEnum(Integer code,String msg){
        this.code=code;
        this.msg=msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
