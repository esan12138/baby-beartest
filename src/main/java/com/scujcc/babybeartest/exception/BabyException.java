package com.scujcc.babybeartest.exception;

/**
 * @Description:
 * @Auther: llang
 */

public class BabyException {
    private Integer code;
    private String msg;

    public BabyException(){

    }

    public BabyException(Integer code,String msg){
        this.code=code;
        this.msg=msg;
    }
}
