package com.scujcc.babybeartest.mapper;

import com.scujcc.babybeartest.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
* @author UserEsan
* @description 针对表【user】的数据库操作Mapper
* @createDate 2022-11-11 15:56:09
* @Entity com.scujcc.pojo.User
*/
@Repository
public interface UserMapper extends BaseMapper<User> {

}




