package com.scujcc.babybeartest.service;

import com.scujcc.babybeartest.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author UserEsan
* @description 针对表【user】的数据库操作Service
* @createDate 2022-11-11 15:56:09
*/
public interface UserService extends IService<User> {

}
