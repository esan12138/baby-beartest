package com.scujcc.babybeartest.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scujcc.babybeartest.pojo.User;
import com.scujcc.babybeartest.service.UserService;
import com.scujcc.babybeartest.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
* @author UserEsan
* @description 针对表【user】的数据库操作Service实现
* @createDate 2022-11-11 15:56:09
*/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService{

}




