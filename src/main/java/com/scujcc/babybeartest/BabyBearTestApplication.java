package com.scujcc.babybeartest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.scujcc.babybeartest.mapper")
public class BabyBearTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(BabyBearTestApplication.class, args);
    }

}
