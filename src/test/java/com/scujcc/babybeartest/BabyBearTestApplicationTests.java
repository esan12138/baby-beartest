package com.scujcc.babybeartest;

import com.scujcc.babybeartest.mapper.UserMapper;
import com.scujcc.babybeartest.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class BabyBearTestApplicationTests {
    @Autowired
    UserMapper userMapper;

    @Test
    void contextLoads() {

    }

    @Test
    void testinsert(){
        User user = new User();
        user.setUserName("usertest1");
        user.setPassword("usertest1");
        int insert = userMapper.insert(user);
        System.out.println(insert);
    }

    @Test
    void testquery(){
        User user = userMapper.selectById(1);
        System.out.println(user);
    }



}
