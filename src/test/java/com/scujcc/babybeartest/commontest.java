package com.scujcc.babybeartest;

import com.scujcc.babybeartest.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @Description:
 * @Date:2022-11-11-16:27
 * @author:Esan
 */
@SpringBootTest
public class commontest {
    @Autowired
    UserMapper userMapper;

    @Test
    void testdelete(){
        int i = userMapper.deleteById(2);
        System.out.println(i);
    }

}
